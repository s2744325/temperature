package nl.utwente.di.temperature;

import java.io.*;
import javax.servlet.*;
import javax.servlet.http.*;

public class Temperature extends HttpServlet {
 	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private TemperatureTranslator translator;
	
    public void init() throws ServletException {
        translator = new TemperatureTranslator();
    }	
	
  public void doGet(HttpServletRequest request,
                    HttpServletResponse response)
      throws ServletException, IOException {

    response.setContentType("text/html");
    PrintWriter out = response.getWriter();
    String docType =
      "<!DOCTYPE HTML>\n";
    String title = "Cellcius to Fh";
    out.println(docType +
                "<HTML>\n" +
                "<HEAD><TITLE>" + title + "</TITLE>" +
                "<LINK REL=STYLESHEET HREF=\"styles.css\">" +
                		"</HEAD>\n" +
                "<BODY BGCOLOR=\"#FDF5E6\">\n" +
                "<H1>" + title + "</H1>\n" +              
                "  <P>Celcius temp: " +
                   request.getParameter("Celcius") + "\n" +
                "  <P>Temp in Fh: " +
                   Double.toString(translator.CtoFg(Double.parseDouble(request.getParameter("Celcius")))) +
                "</BODY></HTML>");
  }
  

}
