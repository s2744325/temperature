package nl.utwente.di.temperature;

import java.util.HashMap;

public class TemperatureTranslator {
    public double CtoFg(double celc) {
        return celc * 1.8 + 32.0;
    }
}
