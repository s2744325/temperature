package nl.utwente.di.temperature;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class TestQuoter {

    @Test
    public void testBook1() throws Exception {
        TemperatureTranslator translator = new TemperatureTranslator();
        double temp = translator.CtoFg(0);
        Assertions.assertEquals(32.0, temp, 0.0, "PRice of book 1");
    }

}
